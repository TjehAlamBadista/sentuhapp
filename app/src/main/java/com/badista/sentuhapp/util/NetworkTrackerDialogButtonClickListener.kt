package com.badista.sentuhapp.util

interface NetworkTrackerDialogButtonClickListener {
    fun onNetworkOKClicked()
    fun onNetworkNoClicked()
}