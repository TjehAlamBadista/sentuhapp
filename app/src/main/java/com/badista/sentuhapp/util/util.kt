package com.badista.sentuhapp.util

import android.content.Context
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.badista.sentuhapp.R
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Locale

fun formattedDate(date: String): String {
    val inputDateString = date
    val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", Locale.getDefault())
    val outputFormat = SimpleDateFormat("dd MMMM yyyy HH:mm", Locale("id", "ID"))

    val date = inputFormat.parse(inputDateString)
    val formattedDate = outputFormat.format(date!!)

    return (formattedDate)
}

fun handleErrorRepository(t: Throwable?): String? {
    val resultErr = try {
        if (t is HttpException) {
            val error: HttpException = t
            val msgs = t.message!!

            if (msgs == "timeout") {
                "Timeout"
            } else {
                if (error.code() >= 500) {
                    "Terjadi Kesalahan Pada Server"
                } else {
                    val jObjError = JSONObject(error.response()?.errorBody()!!.string())
                    if (jObjError.has("errors")) {
                        "Terjadi Kesalahan"
                    } else {
                        if (jObjError.has("message")) {
                            val jsonMsg = jObjError.getString("message")
                            jsonMsg.ifEmpty { "Terjadi Kesalahan Pada Server" }
                        } else {
                            "Terjadi Kesalahan Pada Server"
                        }
                    }
                }
            }
        } else {
            "Terjadi Kesalahan Pada Server"
        }
    } catch (e: IOException) {
        "Pastikan internet terhubung"
    } catch (e: Exception) {
        "Terjadi Kesalahan Pada Server"
    }
    return resultErr
}

fun hideSoftKeyboard(view: View) {
    val inputMethodManager = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun defaultProductOperator(): RequestOptions {
    return RequestOptions()
        .centerCrop()
        .placeholder(R.drawable.ic_launcher_foreground)
        .error(R.drawable.ic_launcher_foreground)
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .skipMemoryCache(true)
        .priority(Priority.HIGH)
}

fun imageRequestOption(): RequestOptions {
    return RequestOptions()
        .centerCrop()
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .skipMemoryCache(true)
        .priority(Priority.HIGH)
}

fun getVerticalManager(context: Context): LinearLayoutManager {
    return  LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
}

fun validateJokes(jokes: String): Boolean {
    if (jokes.isNotEmpty()) {
        return true
    }
    return false
}