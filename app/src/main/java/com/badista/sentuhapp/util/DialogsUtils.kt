package com.badista.sentuhapp.util

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.ImageView
import com.badista.sentuhapp.R
import com.badista.sentuhapp.databinding.BottomSheetDialogBinding
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog

class DialogsUtils(val mContext: Context) {

    val mNetworkDialog = BottomSheetDialog(mContext)
    @SuppressLint("SetTextI18n")
    fun openAlertDialogNetwork(listener: NetworkTrackerDialogButtonClickListener) {
        val view = BottomSheetDialogBinding.inflate(LayoutInflater.from(mContext), null, false)
        mNetworkDialog.setCancelable(false)
        Glide.with(mContext).load(R.drawable.img_illustration_internet)
            .apply(imageRequestOption())
            .fitCenter()
            .into(view.imgFeature)
        view.titleText.text = "Aplikasi tidak terhubung ke internet"
        view.subtitleText.text = "Nyalakan koneksimu untuk mengakses aplikasi"
        view.btnAction.text = "Coba Lagi"
        view.imgFeature.scaleType = ImageView.ScaleType.CENTER_CROP
        view.btnAction.setOnClickListener {
            listener.onNetworkOKClicked()
            mNetworkDialog.dismiss()
            mNetworkDialog.cancel()
        }
        mNetworkDialog.setContentView(view.root)

        if (!mNetworkDialog.isShowing) {
            mNetworkDialog.show()
        } else {
            mNetworkDialog.dismiss()
            mNetworkDialog.cancel()
        }
    }
}