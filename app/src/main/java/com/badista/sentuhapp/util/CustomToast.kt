package com.badista.sentuhapp.util

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.badista.sentuhapp.R
import com.badista.sentuhapp.databinding.CustomToastBinding

class CustomToast(val mContext: Context) {

    fun show(param: String, msgStr: String){
        val binding : CustomToastBinding = DataBindingUtil.inflate(
            LayoutInflater.from(mContext),
            R.layout.custom_toast,
            null,
            false
        )

        val message: CharSequence = msgStr
        val duration = Toast.LENGTH_LONG

        binding.tvMessage.text = message

        if(param == "success"){
            binding.image.setImageResource(R.drawable.ic_check)
            binding.image.visibility = View.VISIBLE
            binding.toastLayoutRoot.setBackgroundColor(Color.parseColor("#E2F5EA"))
            binding.tvMessage.setTextColor(Color.parseColor("#27AE60"))
        }
        else if(param == "error"){
            binding.toastLayoutRoot.setBackgroundColor(Color.parseColor("#F2FF8087"))
            binding.tvMessage.setTextColor(Color.parseColor("#FFFFFF"))
        }
        else if(param == "warning"){
            binding.toastLayoutRoot.setBackgroundColor(Color.parseColor("#EDFF931C"))
            binding.tvMessage.setTextColor(Color.parseColor("#FFFFFF"))
        }
        else{
            binding.toastLayoutRoot.setBackgroundColor(Color.parseColor("#E9BDBDBD"))
            binding.tvMessage.setTextColor(Color.parseColor("#E4000000"))
        }

        val toast = Toast(mContext)
        toast.view = binding.root
        toast.duration = duration
        toast.setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL, 0, 0)
        toast.show()
    }
}