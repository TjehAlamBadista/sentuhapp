package com.badista.sentuhapp.di

import com.badista.sentuhapp.data.network.Api
import com.badista.sentuhapp.data.network.ApiService
import com.badista.sentuhapp.repository.HomeRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideApi(): Api = ApiService.getService()

    @Provides
    fun provideHomeRepository() = HomeRepository()
}