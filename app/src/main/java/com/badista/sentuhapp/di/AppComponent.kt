package com.badista.sentuhapp.di

import com.badista.sentuhapp.repository.HomeRepository
import com.badista.sentuhapp.ui.main.viewmodel.HomeViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(homeRepository: HomeRepository)
    fun inject(homeViewModel: HomeViewModel)

}