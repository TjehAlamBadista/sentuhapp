package com.badista.sentuhapp.ui.splash.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.badista.sentuhapp.R
import com.badista.sentuhapp.databinding.ActivitySplashBinding
import com.badista.sentuhapp.ui.main.view.MainActivity
import com.badista.sentuhapp.util.hideSoftKeyboard

class SplashActivity : AppCompatActivity() {


    private lateinit var binding: ActivitySplashBinding

    companion object {
        lateinit var mActivity: SplashActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mActivity = this

        hideSoftKeyboard(binding.root)

        Handler(Looper.getMainLooper()).postDelayed({
            goToActivity()
        }, 3000)
    }

    private fun goToActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}