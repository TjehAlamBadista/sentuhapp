package com.badista.sentuhapp.ui.TestService.view

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.badista.sentuhapp.R
import com.badista.sentuhapp.databinding.ActivityTestBinding
import com.badista.sentuhapp.service.TestService

class TestActivity : AppCompatActivity(), TestService.ServiceCallback {

    private lateinit var binding: ActivityTestBinding

    private lateinit var myService: TestService
    private var isBound = false

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as TestService.LocalBinder
            myService = binder.getService()
            myService.setCallback(this@TestActivity)
            isBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            isBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSentuh.setOnClickListener {
            Intent(this, TestService::class.java).also { intent ->
                bindService(intent, connection, Context.BIND_AUTO_CREATE)
            }
        }

        binding.btnTest.setOnClickListener {
            ServiceActivity.start(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isBound) {
            unbindService(connection)
            isBound = false
        }
    }

    override fun onReceiveMessage(message: String) {
        runOnUiThread {
            val textView: TextView = findViewById(R.id.tv)
            textView.text = message
        }
    }
}