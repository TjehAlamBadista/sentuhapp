package com.badista.sentuhapp.ui.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.badista.sentuhapp.databinding.ItemsJokesBinding
import com.badista.sentuhapp.ui.main.model.SearchResponse
import com.badista.sentuhapp.util.defaultProductOperator
import com.badista.sentuhapp.util.formattedDate
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class JokesAdapter(data: List<SearchResponse.Result>,context: Context): ListAdapter<SearchResponse.Result, JokesAdapter.ViewHolder>(ItemDiffCallback) {
    private val mData: List<SearchResponse.Result> = data
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private val mContext: Context = context
    var onClickItems: ((SearchResponse.Result) -> Unit)? = null

    companion object ItemDiffCallback: DiffUtil.ItemCallback<SearchResponse.Result>() {
        override fun areItemsTheSame(
            oldItem: SearchResponse.Result,
            newItem: SearchResponse.Result
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: SearchResponse.Result,
            newItem: SearchResponse.Result
        ): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemsJokesBinding.inflate(mInflater,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position], mContext)
        holder.itemView.setOnClickListener {
            onClickItems?.invoke(mData[position])
        }
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    class ViewHolder(private val binding: ItemsJokesBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SearchResponse.Result, context: Context) {
            binding.apply {
                tvRandom.text = item.value
                tvDateRandom.text = formattedDate(item.created_at)
                try {
                    Glide.with(context).load(item.icon_url)
                        .apply(defaultProductOperator())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .into(binding.imgRandom)
                } catch (e: OutOfMemoryError) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }
}