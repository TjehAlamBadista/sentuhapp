package com.badista.sentuhapp.ui.main.model

import java.io.Serializable

data class SearchResponse(
    val result: List<Result>,
    val total: Int
): Serializable{

    data class Result(
        val categories: List<String>,
        val created_at: String,
        val icon_url: String,
        val id: String,
        val updated_at: String,
        val url: String,
        val value: String
    )

}
