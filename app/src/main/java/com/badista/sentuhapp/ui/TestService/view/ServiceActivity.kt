package com.badista.sentuhapp.ui.TestService.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.badista.sentuhapp.databinding.ActivityServiceBinding
import com.badista.sentuhapp.service.TestService

class ServiceActivity : AppCompatActivity() {

    private lateinit var binding: ActivityServiceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityServiceBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.sendMessageButton.setOnClickListener {
            val serviceIntent = Intent(this, TestService::class.java)
            serviceIntent.putExtra("message", "Message from Service Activity")
            startService(serviceIntent)
        }
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ServiceActivity::class.java)
            context.startActivity(intent)
        }
    }
}