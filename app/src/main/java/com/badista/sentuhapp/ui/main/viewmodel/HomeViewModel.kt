package com.badista.sentuhapp.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.badista.sentuhapp.di.DaggerAppComponent
import com.badista.sentuhapp.repository.HomeRepository
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel: ViewModel() {

    @Inject
    lateinit var repository: HomeRepository

    private val compositeDisposable = CompositeDisposable()

    init {
        DaggerAppComponent.create().inject(this)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getRandom() {
        viewModelScope.launch {
            compositeDisposable.add(repository.getRandom())
        }
    }

    fun getRandomCategories(category: String) {
        viewModelScope.launch {
            compositeDisposable.add(repository.getRandomCategories(category))
        }
    }

    fun getSearch(query: String) {
        viewModelScope.launch {
            compositeDisposable.add(repository.getSearch(query))
        }
    }

}