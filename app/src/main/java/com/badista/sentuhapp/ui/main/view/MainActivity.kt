package com.badista.sentuhapp.ui.main.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.badista.sentuhapp.R
import com.badista.sentuhapp.databinding.ActivityMainBinding
import com.badista.sentuhapp.databinding.BottomSheetCategoriesBinding
import com.badista.sentuhapp.ui.TestService.view.TestActivity
import com.badista.sentuhapp.ui.main.adapter.JokesAdapter
import com.badista.sentuhapp.ui.main.model.SearchResponse
import com.badista.sentuhapp.ui.main.viewmodel.HomeViewModel
import com.badista.sentuhapp.util.ConnectionType
import com.badista.sentuhapp.util.CustomToast
import com.badista.sentuhapp.util.DefaultOnlineChecker
import com.badista.sentuhapp.util.DialogsUtils
import com.badista.sentuhapp.util.NetworkTrackerDialogButtonClickListener
import com.badista.sentuhapp.util.NetworkUtil
import com.badista.sentuhapp.util.defaultProductOperator
import com.badista.sentuhapp.util.formattedDate
import com.badista.sentuhapp.util.getVerticalManager
import com.badista.sentuhapp.util.hideSoftKeyboard
import com.badista.sentuhapp.util.validateJokes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.ajalt.timberkt.Timber
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity(), NetworkTrackerDialogButtonClickListener {

    private lateinit var binding: ActivityMainBinding

    private val mDataJokes = mutableListOf<SearchResponse.Result>()

    private lateinit var adapterJokes: JokesAdapter
    private val viewModel = HomeViewModel()
    private val networkMonitor = NetworkUtil(this)

    private var doubleBackToExitPressedOnce = false

    private var parCategoriesValue = ""
    private var page = 1
    private var getScrollBottom = false

    private val parBsFilterSortby by lazy {
        BottomSheetCategoriesBinding.inflate(LayoutInflater.from(this), null, false)
    }

    private val parBsdFilterSortby by lazy {
        BottomSheetDialog(this, R.style.Theme_Design_BottomSheetDialog).apply {
            setCanceledOnTouchOutside(false)
            setContentView(parBsFilterSortby.root)
        }
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            onBackPress()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        checkConnectionNetwork()

        adapterJokes = JokesAdapter(mDataJokes, this)
        onBackPressedDispatcher.addCallback(onBackPressedCallback)

        initUi()
        initAction()
        initData()
    }

    private fun initUi() {
        binding.rvList.apply {
            layoutManager = getVerticalManager(this@MainActivity)
            adapter = adapterJokes
        }
    }

    private fun initAction() {
        binding.btnRandom.setOnClickListener {
            binding.btnCategories.setBackgroundResource(R.drawable.btn_white_border_primary_round_small)
            binding.tvCategories.apply {
                text = "Filter"
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    setTextAppearance(R.style.textSemiBoldPrimary)
                }else{
                    setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                }
            }
            if (networkMonitor()){
                modelRandom()
            }
        }

        binding.btnSearch.setOnClickListener {
            binding.btnCategories.setBackgroundResource(R.drawable.btn_white_border_primary_round_small)
            binding.tvCategories.apply {
                text = "Filter"
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    setTextAppearance(R.style.textSemiBoldPrimary)
                }else{
                    setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                }
            }
            hideSoftKeyboard(binding.root)
            if (networkMonitor()){
                Handler(Looper.getMainLooper()).postDelayed({
                    setLoading(true)
                    modelSearchJokes()
                    binding.inputJokes.setText("")
                }, 500)
            }
        }

        binding.inputJokes.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateForm()
            }
            override fun afterTextChanged(s: Editable?) {}
        })

        binding.rvList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !getScrollBottom && page != 0 && page > 1) {
                    getScrollBottom = true
                }
            }
        })

        binding.btnCategories.setOnClickListener {
            bsSort()
        }
        binding.btnPart1.setOnClickListener {
            startActivity(Intent(this, TestActivity::class.java))
        }
    }

    private fun initData() {
        initDataRandom()
        initDataRandomCategories()
        initDataSearchJokes()
    }

    private fun modelRandom() {
        mDataJokes.clear()
        adapterJokes.notifyDataSetChanged()

        viewModel.getRandom()
    }

    private fun initDataRandom() {
        viewModel.repository.dataRandom.observe(this) { giphies ->
            giphies.let {
                setLoading(false)
                setEmpty(false)
                binding.apply {
                    layoutContentRandom.visibility = View.VISIBLE
                    layoutTotalJokes.visibility = View.GONE

                    tvRandom.text = it.value
                    tvDateRandom.text = formattedDate(it.created_at)
                    try {
                        Glide.with(this@MainActivity).load(it.icon_url)
                            .apply(defaultProductOperator())
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(binding.imgRandom)
                    } catch (e: OutOfMemoryError) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }

        viewModel.repository.onProgress.observe(this){ isLoading ->
            isLoading.let {
                if (isLoading){
                    setLoading(true)
                } else {
                    setLoading(false)
                }
            }
        }

        viewModel.repository.onError.observe(this){
            CustomToast(this).show("error", it)
            setLoading(false)
            setEmpty(true)
        }
    }

    private fun modelRandomCategories(category: String) {
        mDataJokes.clear()
        adapterJokes.notifyDataSetChanged()

        viewModel.getRandomCategories(category)
    }

    private fun initDataRandomCategories() {
        viewModel.repository.dataRandomCategories.observe(this) { giphies ->
            giphies.let {
                setLoading(false)
                setEmpty(false)
                binding.apply {
                    CustomToast(this@MainActivity).show("success", "Filter Kategori $parCategoriesValue Berhasil")
                    layoutContentRandom.visibility = View.VISIBLE
                    layoutTotalJokes.visibility = View.GONE

                    tvRandom.text = it.value
                    tvDateRandom.text = formattedDate(it.created_at)
                    try {
                        Glide.with(this@MainActivity).load(it.icon_url)
                            .apply(defaultProductOperator())
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                            .into(binding.imgRandom)
                    } catch (e: OutOfMemoryError) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }

        viewModel.repository.onProgress.observe(this){ isLoading ->
            isLoading.let {
                if (isLoading){
                    setLoading(true)
                } else {
                    setLoading(false)
                }
            }
        }

        viewModel.repository.onError.observe(this){
            CustomToast(this).show("error", it)
            setLoading(false)
            setEmpty(true)
        }
    }

    private fun modelSearchJokes(){
        val jokes = binding.inputJokes.text.toString()
        viewModel.getSearch(jokes)
    }

    private fun initDataSearchJokes() {
        viewModel.repository.dataSearch.observe(this) { giphies ->
            giphies.let {
                setLoading(false)
                setEmpty(false)
                setInputText(true)
                if(it.total != 0){
                    val value = it.result
                    parCategoriesValue = ""

                    getScrollBottom = false
                    binding.tvTotalJokes.text = it.total.toString()

                    mDataJokes.addAll(value)
                    adapterJokes.notifyDataSetChanged()
                    setEmpty(false)
                } else {
                    mDataJokes.clear()
                    adapterJokes.notifyDataSetChanged()
                    setLoading(false)
                    setEmpty(true)
                    validateForm()
                    setInputText(true)
                }
            }
        }

        viewModel.repository.onProgress.observe(this){ isLoading ->
            isLoading.let {
                if (isLoading){
                    setLoading(true)
                } else {
                    setLoading(false)
                }
            }
        }

        viewModel.repository.onError.observe(this){
            CustomToast(this).show("error", it)
            setLoading(false)
            setEmpty(true)
            setInputText(true)
            validateForm()
            mDataJokes.clear()
            adapterJokes.notifyDataSetChanged()
        }
    }

    private fun bsSort(){
        try {
            setMenu()

            parBsFilterSortby.btnReset.setOnClickListener {
                parBsdFilterSortby.dismiss()
                parBsdFilterSortby.cancel()

                parCategoriesValue = ""

                binding.layoutContentRandom.visibility = View.GONE
                binding.layoutTotalJokes.visibility = View.GONE
                binding.btnCategories.setBackgroundResource(R.drawable.btn_white_border_primary_round_small)
                binding.tvCategories.apply {
                    text = "Filter"
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setTextAppearance(R.style.textSemiBoldPrimary)
                    }else{
                        setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    }
                }
                mDataJokes.clear()
                adapterJokes.notifyDataSetChanged()
                setEmpty(true)
                setLoading(false)
            }

            parBsFilterSortby.btnAction.setOnClickListener {
                parBsdFilterSortby.dismiss()
                parBsdFilterSortby.cancel()

                binding.btnCategories.setBackgroundResource(R.drawable.btn_selector_primary)
                binding.tvCategories.apply {
                    text = parCategoriesValue
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setTextAppearance(R.style.textSemiBoldWhite)
                    }else{
                        setTextColor(ContextCompat.getColor(context, R.color.white))
                    }
                }
                if (networkMonitor()){
                    modelRandomCategories(parCategoriesValue)
                }
            }
            parBsdFilterSortby.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setMenu(){
        try {
            parBsFilterSortby.animal.setOnClickListener {
                parCategoriesValue = "animal"
                parBsFilterSortby.btnAction.text = "Animal"
            }
            parBsFilterSortby.career.setOnClickListener {
                parCategoriesValue = "career"
                parBsFilterSortby.btnAction.text = "Career"
            }
            parBsFilterSortby.celebrity.setOnClickListener {
                parCategoriesValue = "celebrity"
                parBsFilterSortby.btnAction.text = "Celebrity"
            }
            parBsFilterSortby.dev.setOnClickListener {
                parCategoriesValue = "dev"
                parBsFilterSortby.btnAction.text = "Dev"
            }
            parBsFilterSortby.explicit.setOnClickListener {
                parCategoriesValue = "explicit"
                parBsFilterSortby.btnAction.text = "Explicit"
            }
            parBsFilterSortby.fashion.setOnClickListener {
                parCategoriesValue = "fashion"
                parBsFilterSortby.btnAction.text = "Fashion"
            }
            parBsFilterSortby.history.setOnClickListener {
                parCategoriesValue = "history"
                parBsFilterSortby.btnAction.text = "History"
            }
            parBsFilterSortby.money.setOnClickListener {
                parCategoriesValue = "money"
                parBsFilterSortby.btnAction.text = "Money"
            }
            parBsFilterSortby.movie.setOnClickListener {
                parCategoriesValue = "movie"
                parBsFilterSortby.btnAction.text = "Movie"
            }
            parBsFilterSortby.music.setOnClickListener {
                parCategoriesValue = "music"
                parBsFilterSortby.btnAction.text = "Music"
            }
            parBsFilterSortby.political.setOnClickListener {
                parCategoriesValue = "political"
                parBsFilterSortby.btnAction.text = "Political"
            }
            parBsFilterSortby.religion.setOnClickListener {
                parCategoriesValue = "religion"
                parBsFilterSortby.btnAction.text = "Religion"
            }
            parBsFilterSortby.science.setOnClickListener {
                parCategoriesValue = "science"
                parBsFilterSortby.btnAction.text = "Science"
            }
            parBsFilterSortby.sport.setOnClickListener {
                parCategoriesValue = "sport"
                parBsFilterSortby.btnAction.text = "Sport"
            }
            parBsFilterSortby.travel.setOnClickListener {
                parCategoriesValue = "travel"
                parBsFilterSortby.btnAction.text = "Travel"
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setInputText(value: Boolean) {
        binding.apply {
            inputJokes.isEnabled = value
        }
    }

    private fun validateForm() {
        val jokes = binding.inputJokes.text.toString().trim()
        binding.btnSearch.isEnabled = validateJokes(jokes)
    }

    private fun setEmpty(show: Boolean) {
        if (show) {
            binding.layoutEmpty.visibility = View.VISIBLE
            binding.rvList.visibility = View.GONE
            binding.layoutTotalJokes.visibility = View.GONE
        } else {
            binding.layoutEmpty.visibility = View.GONE
            binding.rvList.visibility = View.VISIBLE
            binding.layoutTotalJokes.visibility = View.VISIBLE
        }
    }

    private fun setLoading(status: Boolean) {
        if (status) {
            binding.loading.visibility = View.VISIBLE
            binding.layoutEmpty.visibility = View.GONE
            binding.layoutContentRandom.visibility = View.GONE
            binding.rvList.visibility = View.GONE
            binding.layoutTotalJokes.visibility = View.GONE
        } else {
            binding.loading.visibility = View.GONE
        }
    }
    fun checkConnectionNetwork() {
        networkMonitor.result = { isAvailable, type ->
            runOnUiThread {
                when (isAvailable) {
                    true -> {
                        when (type) {
                            ConnectionType.Wifi -> {
                                Timber.tag("NETWORK_MONITOR_STATUS").i("Wifi Connection")
                            }

                            ConnectionType.Cellular -> {
                                Timber.tag("NETWORK_MONITOR_STATUS").i("Cellular Connection")
                            }

                            else -> {
                                Timber.tag("NETWORK_MONITOR_STATUS").i("Network Connection")
                            }
                        }
                    }

                    false -> {
                        try {
                            networkMonitor()
                        }
                        catch (e: Exception){
                            e.printStackTrace()
                        }
                    }
                }
            }
        }
        networkMonitor.register()
    }

    private fun unregisterNetworkManger() {
        if (networkMonitor.isRegisterNetwork) {
            networkMonitor.unregister()
        }
    }

    private fun networkMonitor(): Boolean {

        val isNetwork = DefaultOnlineChecker(this)

        if (!isNetwork) {
            if (!DialogsUtils(this).mNetworkDialog.isShowing){
                DialogsUtils(this).openAlertDialogNetwork(this)
            } else {
                DialogsUtils(this).mNetworkDialog.dismiss()
                DialogsUtils(this).mNetworkDialog.cancel()
            }
        }

        return isNetwork
    }

    private fun onBackPress() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity()
            exitProcess(0)
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()
        Handler(Looper.getMainLooper()).postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun onRestart() {
        super.onRestart()
        networkMonitor.register()
    }

    override fun onPause() {
        super.onPause()
        unregisterNetworkManger()
    }

    override fun onStop() {
        super.onStop()
        unregisterNetworkManger()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterNetworkManger()
    }

    private fun refreshActivity(){
        this.recreate()
    }

    override fun onNetworkOKClicked() {
        onResume()
        refreshActivity()
    }

    override fun onNetworkNoClicked() {}
}