package com.badista.sentuhapp

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import timber.log.Timber

private const val TAG = "BASE_APPLICATION"
class BaseApplication: Application(), Application.ActivityLifecycleCallbacks {
    companion object {
        lateinit var instance: BaseApplication

        private var activity: Activity? = null

        fun getActivity(): Activity? {
            return activity
        }
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        instance = this

        registerActivityLifecycleCallbacks(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        Timber.tag(TAG).e("onActivityCreated")
    }

    override fun onActivityStarted(activity: Activity) {
        Timber.tag(TAG).e("onActivityStarted")
    }

    override fun onActivityResumed(activity: Activity) {
        Timber.tag(TAG).e("onActivityResumed")
    }

    override fun onActivityPaused(activity: Activity) {
        Timber.tag(TAG).e("onActivityPaused")
    }

    override fun onActivityStopped(activity: Activity) {
        Timber.tag(TAG).e("onActivityStopped")
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        Timber.tag(TAG).e("onActivitySaveInstanceState")
    }

    override fun onActivityDestroyed(activity: Activity) {
        Timber.tag(TAG).e("onActivityDestroyed")
    }
}