package com.badista.sentuhapp.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.badista.sentuhapp.util.CustomToast

class TestService : Service() {

    private val binder = LocalBinder()
    private var callback: ServiceCallback? = null

    inner class LocalBinder : Binder() {
        fun getService(): TestService = this@TestService
    }

    interface ServiceCallback {
        fun onReceiveMessage(message: String)
    }

    fun setCallback(callback: ServiceCallback) {
        this.callback = callback
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        CustomToast(this).show("success", "Service Started")

        Thread {
            Thread.sleep(3000)
            callback?.onReceiveMessage("Message from Service")
        }.start()

        return START_NOT_STICKY
    }
}