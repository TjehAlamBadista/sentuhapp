package com.badista.sentuhapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.badista.sentuhapp.data.network.Api
import com.badista.sentuhapp.di.DaggerAppComponent
import com.badista.sentuhapp.ui.main.model.RandomResponse
import com.badista.sentuhapp.ui.main.model.SearchResponse
import com.badista.sentuhapp.util.handleErrorRepository
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import timber.log.Timber
import javax.inject.Inject

class HomeRepository {

    @Inject
    lateinit var apiService: Api

    private val _dataRandom = MutableLiveData<RandomResponse>()
    val dataRandom: LiveData<RandomResponse> get() = _dataRandom

    private val _dataRandomCategories = MutableLiveData<RandomResponse>()
    val dataRandomCategories: LiveData<RandomResponse> get() = _dataRandomCategories

    private val _dataSearch = MutableLiveData<SearchResponse>()
    val dataSearch: LiveData<SearchResponse> get() = _dataSearch

    private val _onError by lazy { MutableLiveData<String>() }
    val onError: LiveData<String> get() = _onError

    private val _onProgress by lazy { MutableLiveData<Boolean>() }
    val onProgress: LiveData<Boolean> get() = _onProgress

    init {
        DaggerAppComponent.create().inject(this)
    }

    fun getRandom(): Disposable {
        _onProgress.postValue(true)

        return apiService.getRandom()
            .subscribeOn(Schedulers.io())
            .subscribeWith(subscribeToServerRandom())
    }

    private fun subscribeToServerRandom(): DisposableSubscriber<RandomResponse> {
        return object: DisposableSubscriber<RandomResponse>() {
            override fun onNext(t: RandomResponse?) {
                Timber.tag("subscribeToServer").i("On Next")
                if (t != null) {
                    _dataRandom.postValue(t)
                    _onProgress.postValue(false)
                } else {
                    Timber.tag("subscribeToServer").i("On Next => null object")
                    _onProgress.postValue(false)
                }
            }

            override fun onError(t: Throwable?) {
                Timber.tag("subscribeToServer").i("onError: " + t?.message)
                _onProgress.postValue(false)
                _onError.postValue(handleErrorRepository(t))
            }

            override fun onComplete() {
                Timber.tag("subscribeToServer").i("onComplete")
                _onProgress.postValue(false)
            }
        }
    }

    fun getRandomCategories(category: String?): Disposable {
        _onProgress.postValue(true)

        return apiService.getRandomCategories(category!!)
            .subscribeOn(Schedulers.io())
            .subscribeWith(subscribeToServerRandomCategories())
    }

    private fun subscribeToServerRandomCategories(): DisposableSubscriber<RandomResponse> {
        return object: DisposableSubscriber<RandomResponse>() {
            override fun onNext(t: RandomResponse?) {
                Timber.tag("subscribeToServer").i("On Next")
                if (t != null) {
                    _dataRandomCategories.postValue(t)
                    _onProgress.postValue(false)
                } else {
                    Timber.tag("subscribeToServer").i("On Next => null object")
                    _onProgress.postValue(false)
                }
            }

            override fun onError(t: Throwable?) {
                Timber.tag("subscribeToServer").i("onError: " + t?.message)
                _onProgress.postValue(false)
                _onError.postValue(handleErrorRepository(t))
            }

            override fun onComplete() {
                Timber.tag("subscribeToServer").i("onComplete")
                _onProgress.postValue(false)
            }
        }
    }

    fun getSearch(query: String): Disposable {
        _onProgress.postValue(true)

        return apiService.getSearch(query)
            .subscribeOn(Schedulers.io())
            .subscribeWith(subscribeToServerSearch())
    }

    private fun subscribeToServerSearch(): DisposableSubscriber<SearchResponse> {
        return object: DisposableSubscriber<SearchResponse>() {
            override fun onNext(t: SearchResponse?) {
                Timber.tag("subscribeToServer").i("On Next")
                if (t != null) {
                    _dataSearch.postValue(t)
                    _onProgress.postValue(false)
                } else {
                    Timber.tag("subscribeToServer").i("On Next => null object")
                    _onProgress.postValue(false)
                }
            }

            override fun onError(t: Throwable?) {
                Timber.tag("subscribeToServer").i("onError: " + t?.message)
                _onProgress.postValue(false)
                _onError.postValue(handleErrorRepository(t))
            }

            override fun onComplete() {
                Timber.tag("subscribeToServer").i("onComplete")
                _onProgress.postValue(false)
            }
        }
    }
}