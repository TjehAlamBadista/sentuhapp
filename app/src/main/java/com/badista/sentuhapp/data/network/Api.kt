package com.badista.sentuhapp.data.network

import com.badista.sentuhapp.ui.main.model.CategoryResponse
import com.badista.sentuhapp.ui.main.model.RandomResponse
import com.badista.sentuhapp.ui.main.model.SearchResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET(Constants.GET_RANDOM)
    fun getRandom(): Flowable<RandomResponse>

    @GET(Constants.GET_SEARCH)
    fun getSearch(
        @Query("query") query:String
    ): Flowable<SearchResponse>

    @GET(Constants.GET_RANDOM)
    fun getRandomCategories(
        @Query("category") category:String
    ): Flowable<RandomResponse>
}