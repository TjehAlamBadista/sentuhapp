package com.badista.sentuhapp.data.network

import android.util.Base64
import com.badista.sentuhapp.BuildConfig
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.io.IOException
import java.lang.reflect.Type
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit

object ApiService {
    abstract class MoshiArrayListJsonAdapter<C : MutableCollection<T>?, T> private constructor(
        private val elementAdapter: JsonAdapter<T>
    ) :
        JsonAdapter<C>() {
        abstract fun newCollection(): C

        @Throws(IOException::class)
        override fun fromJson(reader: JsonReader): C {
            val result = newCollection()
            reader.beginArray()
            while (reader.hasNext()) {
                result?.add(elementAdapter.fromJson(reader)!!)
            }
            reader.endArray()
            return result
        }

        @Throws(IOException::class)
        override fun toJson(writer: JsonWriter, value: C?) {
            writer.beginArray()
            for (element in value!!) {
                elementAdapter.toJson(writer, element)
            }
            writer.endArray()
        }

        override fun toString(): String {
            return "$elementAdapter.collection()"
        }

        companion object {
            val FACTORY = Factory { type, annotations, moshi ->
                val rawType = Types.getRawType(type)
                if (annotations.isNotEmpty()) return@Factory null
                if (rawType == ArrayList::class.java) {
                    return@Factory newArrayListAdapter<Any>(
                        type,
                        moshi
                    ).nullSafe()
                }
                null
            }

            private fun <T> newArrayListAdapter(
                type: Type,
                moshi: Moshi
            ): JsonAdapter<MutableCollection<T>> {
                val elementType =
                    Types.collectionElementType(
                        type,
                        MutableCollection::class.java
                    )

                val elementAdapter: JsonAdapter<T> = moshi.adapter(elementType)

                return object :
                    MoshiArrayListJsonAdapter<MutableCollection<T>, T>(elementAdapter) {
                    override fun newCollection(): MutableCollection<T> {
                        return ArrayList()
                    }
                }
            }
        }
    }

    private fun createOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor {
                message -> Timber.tag("RetrofitResponse").i(message)
        }
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
//            .addInterceptor(AuthInterceptor())
            .addInterceptor(loggingInterceptor)
            .readTimeout(120, TimeUnit.SECONDS)
            .connectTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()
    }

    private lateinit var apiService: Api

    var moshi: Moshi = Moshi.Builder()
        .add(MoshiArrayListJsonAdapter.FACTORY)
        .build()

    fun getService(): Api {
        if (!::apiService.isInitialized) {
            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.API_KEY)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .client(createOkHttpClient())
                .build()

            apiService = retrofit.create(Api::class.java)
        }

        return apiService
    }
}