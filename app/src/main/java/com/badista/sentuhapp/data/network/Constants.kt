package com.badista.sentuhapp.data.network

object Constants {

    const val GET_RANDOM = "random"
    const val GET_SEARCH = "search"
}